const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

const common = {
    entry: path.resolve(__dirname, 'src/index.tsx'),
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    module: {
        rules: [
            {
                test: /\.(ts|js)x?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader'
                    }
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]?[hash]'
                        }
                    },
                ],
            }
        ]
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].bundle.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, 'public/index.html')
        }),
        new FaviconsWebpackPlugin({
            logo: path.resolve(__dirname, 'public/logo192.png'),
            mode: 'webapp',
            cache: true,
        })
    ],
}

module.exports = (env) => {
    console.log("env", env);
    if (env.production) {
        return {
            ...common,
            mode: 'production',
        }
    }
    else {
        return {
            ...common,
            mode: 'development',
            devServer: {
                //contentBase: path.join(__dirname, '/'),
                filename: '[name].bundle.js',
                //inline: false,
                historyApiFallback: true,
                open: true,
                hot: true,
                port: 3000,
            },
            optimization: {
                runtimeChunk: 'single'
            },
            devtool: 'inline-source-map'
        }
    }
};