import React, { useEffect } from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { IProduct, Products } from './data/data'
import products from './data/products.json'
import { getAllProducts } from './redux/reducers/product';
import ProductList from './components/product/ProductList';
import BasketList from './components/basket/BasketList';
import ProductPage from './components/product/ProductPage';
import App from './App'


function MyRouter() {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getAllProducts(JSON.parse(JSON.stringify(products))));
        return () => {
            // Очистить подписку
        };
    }, []);

    return (
        <Router>
            <App />
            <Switch>
                <Route exact path="/" component={ProductList} />
                <Route exact path="/Backet" component={BasketList} />
                <Route exact path="/Product/:id" component={ProductPage} />
                <Route component={() => <h1>Other</h1>} />
            </Switch>            
        </Router>
    );
}

export default MyRouter;
