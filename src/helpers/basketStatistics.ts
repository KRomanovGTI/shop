import { IBasketItem } from '../redux/reducers/basket';
export interface IBasketStattistics {
    Count: number;
    Prise: number;    
}

export const getBasketStatistics = (basket: any) => {
    if (basket.Items.lenght === 0)
    {
        return {
            Count: 0,
            Prise: 0
        }
    }
    let Count: number = 0;
    let Prise: number = 0;

    basket.Items.forEach((Item: IBasketItem) => {
        Count += Item.Count;
        Prise += Item.Product.Price * Item.Count;
    });
    return {
        Count: Count,
        Prise: Prise
    }
}