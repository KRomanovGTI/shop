import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { deleteProduct } from '../../redux/reducers/product';
import { addProductToBasket } from '../../redux/reducers/basket';
import { IProduct } from '../../data/data'
import { Paper, IconButton } from '@material-ui/core';
import { Close, AddShoppingCart } from '@material-ui/icons';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { euroFormat } from '../../helpers/moneyFormat';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            width: '300px',
            margin: '20px',
        },
        block: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between',
            paddingLeft: '10px',
            paddingRight: '10px',
        },
        img: {
            display: 'flex',
            marginLeft: 'auto',
            marginRight: 'auto'
        },
        link: {
            textDecoration: 'none',
            color: 'inherit'
        }
    }),
);

const ProductItem: React.FC<{ key: number, Product: IProduct }> = ({ Product }) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const user = useSelector((state: any) => state.user);

    return (
        <Paper className={classes.paper}>
            <div className={classes.block}>
                <Link className={classes.link} to={`/Product/${Product.Id}`}>
                    <h5>{Product.Name}</h5>
                </Link>
                {user.Right === 'admin' && <IconButton color="secondary" onClick={() => dispatch(deleteProduct(Product.Id))}>
                    <Close />
                </IconButton>}
            </div>
            <Link className={classes.link} to={`/Product/${Product.Id}`}>
                <img className={classes.img} src={'../../../image/basket.png'} alt={"Product"} />
            </Link>
            <div className={classes.block}>
                <Link className={classes.link} to={`/Product/${Product.Id}`}>
                    <h3>{euroFormat(Product.Price)}</h3>
                </Link>
                <IconButton color="primary" onClick={() => dispatch(addProductToBasket({ Product, Count: 1 }))}>
                    <AddShoppingCart />
                </IconButton>
            </div>
        </Paper>
    );
}

export default ProductItem;