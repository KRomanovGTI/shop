import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { IProduct, Products } from '../../data/data'
import { Grid, Button, CircularProgress } from '@material-ui/core';
import ProductItem from './ProductItem';
import { euroFormat } from '../../helpers/moneyFormat'


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        marginTop: {
            marginTop: '80px'
        },
        link: {
            textDecoration: 'none',
            color: 'inherit'
        },
        progress: {
            display: 'flex',
            position: 'fixed',
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center'
        }
    }),
);

const ProductList: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector((state: any) => state.user);
    const product = useSelector((state: any) => state.product);



    return (
        product.IsLoading ? <div className={classes.progress}>
            <CircularProgress />
        </div> :
            <div className={classes.marginTop}>
                {user.Right === 'admin' && <Grid container justify="center">
                    <Link className={classes.link} to={`/Product/0`}>
                        <Button>Add new product</Button>
                    </Link>
                </Grid>}
                <Grid container justify="center" spacing={5}>
                    {product.Items.filter((item: IProduct) => item.Name.includes(product.Filter) || euroFormat(item.Price).includes(product.Filter)).map((item: IProduct) => (
                        <ProductItem key={item.Id} Product={item} />
                    ))}
                </Grid>
            </div>
    );

}

export default ProductList;