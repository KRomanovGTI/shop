import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { deleteProduct, addProduct } from '../../redux/reducers/product';
import { addProductToBasket } from '../../redux/reducers/basket';
import { IProduct } from '../../data/data'
import MuiAlert, { AlertProps } from '@material-ui/lab/Alert';
import { Paper, IconButton, Container, Grid, InputBase, TextField, Button, Snackbar } from '@material-ui/core';
import { Close, AddShoppingCart } from '@material-ui/icons';
import { fade, makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { euroFormat } from '../../helpers/moneyFormat';

function Alert(props: AlertProps) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        marginTop: {
            marginTop: '100px'
        },
        block: {
            display: 'flex',
            alignItems: 'flex-start',
            margin: '20px'

        },
        price: {
            margin: '0',
            padding: '12px'
        },
        inputPosition: {
            display: 'flex'
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%'
        },
        buttomSave: {
            display: 'flex',
            justifyContent: 'space-evenly'
        },
        inputStyle: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: fade(theme.palette.common.white, 0.15),
            '&:hover': {
                backgroundColor: fade(theme.palette.common.white, 0.25),
            },
            marginRight: theme.spacing(2),
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(3),
                width: 'auto',
            },
        },
    }),
);

const AdminProductPage: React.FC<{ match: any }> = ({ match }) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const product = useSelector((state: any) => state.product);
    const CurrentItem: IProduct = product.Items.find((item: IProduct) => item.Id === parseInt(match.params.id));
    const NewProduct: IProduct = { Id: 0, Name: '', Price: 0, Description: '' };

    const [Product, setProduct] = useState(CurrentItem === undefined ? NewProduct : CurrentItem);
    const [ShowSnackbar, setShowSnackbar] = useState(false);

    return <Container className={classes.marginTop}>
        <Grid container spacing={5}>
            <img style={{flex: 0}} src={'../../../image/basket.png'} alt={"Product"} />
            <div style={{flex: 1}}>
                <div className={classes.inputPosition}>
                    <h2 className={classes.price}>Name:</h2>
                    <InputBase
                        fullWidth
                        value={Product.Name}
                        onChange={(props: any) => setProduct({ ...Product, Name: props.target.value })}
                    />
                    <hr style={{border: '2px solid blue'}} />
                </div>
                <div className={classes.inputPosition}>
                    <h2 className={classes.price}>Price:</h2>
                    <InputBase
                        type='number'
                        fullWidth
                        value={Product.Price}
                        onChange={(props: any) => setProduct({ ...Product, Price: parseFloat(props.target.value) })}
                    />
                    <hr/>
                </div>
                <div className={classes.inputPosition}>
                    <h2 className={classes.price}>Description:</h2>
                    <InputBase
                        fullWidth
                        value={Product.Description}
                        onChange={(props: any) => setProduct({ ...Product, Description: props.target.value })}
                    />
                    <hr/>
                </div>
                <div className={classes.buttomSave}>
                    <Button variant="outlined" color="primary" disabled={ShowSnackbar} onClick={() => {
                        dispatch(addProduct(Product));
                        setShowSnackbar(true);
                    }}>Save</Button>
                </div>
            </div>
        </Grid>
        <Snackbar open={ShowSnackbar} autoHideDuration={6000} onClose={() => setShowSnackbar(false)}>
            <Alert onClose={() => setShowSnackbar(false)} severity="success">
                This is a success message!
                </Alert>
        </Snackbar>
    </Container>
}

const UserProductPage: React.FC<{ match: any }> = ({ match }) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const product = useSelector((state: any) => state.product);
    const CurrentItem: IProduct = product.Items.find((item: IProduct) => item.Id === parseInt(match.params.id));

    return CurrentItem !== undefined ? <Container className={classes.marginTop}>
        <h1>{CurrentItem.Name}</h1>
        <Grid container spacing={5}>
            <img src={'../../../image/basket.png'} alt={"Product"} />
            <div>
                <div className={classes.block}>
                    <h2 className={classes.price}>{euroFormat(CurrentItem.Price)}</h2>
                    <IconButton color="primary" onClick={() => dispatch(addProductToBasket({ Product: CurrentItem, Count: 1 }))}>
                        <AddShoppingCart />
                    </IconButton>
                </div>
                <h1 className={classes.price}>{`Description: ${CurrentItem.Description}`}</h1>
            </div>
        </Grid>
    </Container> : <div />
}

const ProductPage: React.FC<{ match: any }> = ({ match }) => {
    const user = useSelector((state: any) => state.user);
    return (
        user.Right === 'admin' ? <AdminProductPage match={match} /> : <UserProductPage match={match} />
    );
}

export default ProductPage;
