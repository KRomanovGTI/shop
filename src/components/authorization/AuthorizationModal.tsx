import React, { useState, useEffect, useRef } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { useDispatch, useSelector } from 'react-redux';
import { setUser } from '../../redux/reducers/user';
import { Modal, InputBase, Button } from '@material-ui/core';
import { ContactsOutlined } from '@material-ui/icons';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        modal: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        paper: {
            backgroundColor: theme.palette.background.paper,
            border: '2px solid #000',
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
        inputRoot: {
            color: 'inherit'
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('md')]: {
                width: '20ch'
            },
        },
        align: {
            display: 'flex',
            flexWrap: 'nowrap',
            alignContent: 'center',
            justifyContent: 'space-between',
            alignItems: 'center',
        }
    }),
);

const AuthorizationModal: React.FC<{ open: boolean, handleClose: any }> = ({ open, handleClose }) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const user = useSelector((state: any) => state.user);
    const [userName, setUserName] = useState('');

    useEffect(() => {
        user.User && user.User.Name && setUserName(user.User.Name);
    }, []);

    return (
        <Modal
            className={classes.modal}
            open={open}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <div className={classes.paper}>
                <h3>Autorizations:</h3>
                <div className={classes.align}>
                    <InputBase
                        placeholder="Log in…"
                        autoFocus
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                        value={userName}
                        onChange={(props) => setUserName(props.target.value)}
                    />
                    <Button variant="contained" color="primary" onClick={() => {
                        dispatch(setUser({ Name: userName }));
                        handleClose();
                    }}>
                        Log in
                </Button>
                </div>
            </div>
        </Modal>
    );

}

export default AuthorizationModal;