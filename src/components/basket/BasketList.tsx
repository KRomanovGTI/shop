import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper, IconButton, List, ListItem, ListItemText } from '@material-ui/core';
import { IBasketStattistics, getBasketStatistics } from '../../helpers/basketStatistics';
import { Close } from '@material-ui/icons';
import { deleteProductFromBasket, clearBasket } from '../../redux/reducers/basket'
import { euroFormat } from '../../helpers/moneyFormat';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        table: {
            minWidth: 650,
        },
        marginTop: {
            marginTop: '80px'
        },
        root: {
            display: 'flex',
            width: '100%',
            backgroundColor: '#d0e8f7',
        }
    }),
);

const BasketList: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const basket = useSelector((state: any) => state.basket);
    const statistics: IBasketStattistics = getBasketStatistics(basket);

    return (
        basket.Items.length === 0 ? <h1 className={classes.marginTop}>Basket empty</h1> :
            <div className={classes.marginTop}>
                <List component="nav" className={classes.root}>
                    <ListItem>
                        <ListItemText primary={`Count ${statistics.Count}`} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary={`Average price ${euroFormat(statistics.Prise / statistics.Count)}`} />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary={`Total price ${euroFormat(statistics.Prise)}`} />
                    </ListItem>
                    <ListItem button onClick={()=> dispatch(clearBasket())}>
                        <ListItemText primary="Clear" />
                    </ListItem>
                </List>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Name</TableCell>
                                <TableCell align="right">Prise</TableCell>
                                <TableCell align="right">Count</TableCell>
                                <TableCell align="right">Delete</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {basket.Items.map((row: any) => (
                                <TableRow key={row.Product.Id}>
                                    <TableCell component="th" scope="row">{row.Product.Name}</TableCell>
                                    <TableCell align="right">{euroFormat(row.Product.Price)}</TableCell>
                                    <TableCell align="right">{row.Count}</TableCell>
                                    <TableCell align="right">
                                        <IconButton color="secondary" onClick={() => dispatch(deleteProductFromBasket(row.Product.Id))}>
                                            <Close />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
    );

}

export default BasketList;