import { IAction } from '../helpers/intarface';

interface IUser {
    Name: string
}

interface IDefaulState {
    User: IUser | null;
    Right: 'admin' | 'user' | null;
}

const defaultState: IDefaulState = {
    User: null,
    Right: null,
}

const actionTypes = {
    SET_USER: "SET_USER",
    DELETE_USER: "DELETE_USER",
}

export const setUser = (user: IUser) => ({ type: actionTypes.SET_USER, payload: user });

export const deleteUser = () => ({ type: actionTypes.DELETE_USER });

export default function user(state = defaultState, action: IAction) {
    switch (action.type) {
        case actionTypes.SET_USER:
            return {
                ...state,
                User: action.payload,
                Right: action.payload.Name === 'admin' ? 'admin' : 'user'
            }
        case actionTypes.DELETE_USER:
            return defaultState
        default:
            return state;
    }
}