import { IProduct } from '../../data/data';
import { IAction } from '../helpers/intarface';

interface IDefaulState {
    Items: Array<IProduct>;
    Filter: string;
    IsLoading: boolean;
}

const defaultState: IDefaulState = {
    Items: [],
    Filter: '',
    IsLoading: false,
}

const actionTypes = {
    GET_PRODUCTS: "GET_ALL_PRODUCTS",
    SET_PRODUCT_LOADING: "SET_PRODUCT_LOADING",
    SET_PRODUCT_FILTER: "SET_PRODUCT_FILTER",
    ADD_PRODUCT: "ADD_PRODUCT",
    DELETE_PRODUCT: "DELETE_PRODUCT"
}

export const setProductLoading = (isLoading: boolean) => ({ type: actionTypes.SET_PRODUCT_LOADING, payload: isLoading });

export const setProductFilter = (filter: string) => ({ type: actionTypes.SET_PRODUCT_FILTER, payload: filter });

export const getAllProducts = (products: Array<IProduct>) => ({ type: actionTypes.GET_PRODUCTS, payload: products });

export const addProduct = (product: IProduct) => ({ type: actionTypes.ADD_PRODUCT, payload: product });

export const deleteProduct = (id: number) => ({ type: actionTypes.DELETE_PRODUCT, payload: id });

export default function product(state = defaultState, action: IAction) {
    switch (action.type) {
        case actionTypes.SET_PRODUCT_LOADING:
            return {
                ...state,
                IsLoading: action.payload
            }
        case actionTypes.SET_PRODUCT_FILTER:
            return {
                ...state,
                Filter: action.payload
            }
        case actionTypes.GET_PRODUCTS:
            return {
                ...state,
                Items: action.payload
            }
        case actionTypes.ADD_PRODUCT:
            return {
                ...state,
                Items: state.Items.find((item) => item.Id === action.payload.Id) ? [...state.Items.filter((item) => item.Id !== action.payload.Id), action.payload] : [...state.Items, {...action.payload, Id: action.payload.Id !== 0 ? action.payload.Id : state.Items.length + 2}]
            }
        case actionTypes.DELETE_PRODUCT:
            return {
                ...state,
                Items: state.Items.filter((item) => item.Id !== action.payload)
            }
        default:
            return state;
    }
}