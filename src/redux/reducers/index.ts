import { combineReducers } from 'redux';
import user from './user'
import product from './product';
import basket from './basket';

export default combineReducers({
    user: user,
    product: product,
    basket: basket,
})

