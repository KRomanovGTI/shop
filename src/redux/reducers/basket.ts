import { IProduct } from '../../data/data'
import { IAction } from '../helpers/intarface';

export interface IBasketItem {
    Product: IProduct,
    Count: number
}

interface IDefaulState {
    Items: Array<IBasketItem>;
    IsLoading: boolean;
}

const defaultState: IDefaulState = {
    Items: [],
    IsLoading: false,
}

const actionTypes = {
    SET_BASKET_LOADING: "SET_BASKET_LOADING",
    ADD_PRODUCT_TO_BASKET: "ADD_PRODUCT_TO_BASKET",
    DELETE_PRODUCT_FROM_BASKET: "DELETE_PRODUCT_FROM_BASKET",
    CLEAR_BASKET: "CLEAR_BASKET"
}

export const setBasketLoading = (isLoading: boolean) => ({ type: actionTypes.SET_BASKET_LOADING, payload: isLoading });

export const addProductToBasket = (product: IBasketItem) => ({ type: actionTypes.ADD_PRODUCT_TO_BASKET, payload: product });

export const deleteProductFromBasket = (id: number) => ({ type: actionTypes.DELETE_PRODUCT_FROM_BASKET, payload: id });

export const clearBasket = () => ({ type: actionTypes.CLEAR_BASKET })

export default function product(state = defaultState, action: IAction) {
    switch (action.type) {
        case actionTypes.SET_BASKET_LOADING:
            return {
                ...state,
                IsLoading: action.payload
            }
        case actionTypes.ADD_PRODUCT_TO_BASKET:
            return {
                ...state,
                Items: state.Items.find(item => item.Product.Id === action.payload.Product.Id) ? state.Items.map(item => item.Product.Id === action.payload.Product.Id ? { ...item, Count: item.Count + action.payload.Count } : item) : [...state.Items, action.payload]
            }
        case actionTypes.DELETE_PRODUCT_FROM_BASKET:
            return {
                ...state,
                Items: state.Items.filter((item) => item.Product.Id !== action.payload)
            }
        case actionTypes.CLEAR_BASKET:
            return {
                ...state,
                Items: []
            }
        default:
            return state;
    }
}