import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from './redux/reducers/index';
import App from './App';
import MyRouter from './Router'

const store = createStore(reducer, composeWithDevTools());

render(
  <Provider store={store}>
    <MyRouter />
  </Provider>,
  document.getElementById('root')
);