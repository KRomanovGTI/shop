import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { AppBar, Toolbar, IconButton, Typography, InputBase, Badge, Button } from '@material-ui/core';
import { Store, Search, Person, ShoppingCart } from '@material-ui/icons';
import { fade, makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { setProductFilter } from './redux/reducers/product';
import { IBasketStattistics, getBasketStatistics } from './helpers/basketStatistics';
import { useDispatch, useSelector } from 'react-redux';
import AuthorizationModal from './components/authorization/AuthorizationModal';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    grow: {
      flexGrow: 1,
      justifyContent: 'space-between'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    link: {
      textDecoration: 'none',
      color: 'inherit'
    },
    title: {
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(3),
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('md')]: {
        width: '20ch',
      },
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
  }),
);

function App() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector((state: any) => state.user);
  const basket = useSelector((state: any) => state.basket);
  const statistics: IBasketStattistics = getBasketStatistics(basket);
  const [openAutorizationsModal, setOpenAutorizationsModal] = useState(false);

  return (
    <AppBar>
      <Toolbar className={classes.grow}>
        <div style={{ display: 'flex', alignItems: 'center' }}>
          <Link className={classes.link} to='/'>
            <IconButton
              className={classes.menuButton}
              edge="start"
              color="inherit"
              aria-label="open drawer"
            >
              <Store className={classes.menuButton} />
              <Typography variant="h6" noWrap>
                Shop
          </Typography>

            </IconButton>
          </Link>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <Search />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
              onChange={(props) => dispatch(setProductFilter(props.target.value))}
            />
          </div>
        </div>
        <div>
          <IconButton color="inherit" onClick={() => setOpenAutorizationsModal(true)}>
            <Person />
          </IconButton>
          <Link className={classes.link} to='/Backet'>
            <IconButton color="inherit">
              <Badge badgeContent={statistics.Count} color="secondary">
                <ShoppingCart />
              </Badge>
            </IconButton>
          </Link>

        </div>

      </Toolbar>
      {openAutorizationsModal && <AuthorizationModal open={openAutorizationsModal} handleClose={() => setOpenAutorizationsModal(false)} />}
    </AppBar>
  );
}

export default App;
