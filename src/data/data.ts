export interface IProduct {
    Id: number;
    Name: string;
    Price: number;
    Description: string;
}

export const Products: Array<IProduct> = [
    {Id: 1, Name: 'Mercedes-Benz E 200 d', Price: 43318, Description: 'Acceleration 0-100 (km / h) 8,4'},
    {Id: 2, Name: 'Mercedes-Benz E 220 d', Price: 44885, Description: 'Acceleration 0-100 (km / h) 7,3'},
    {Id: 3, Name: 'Mercedes-Benz E 220 d 4MATIC', Price: 47231, Description: 'Acceleration 0-100 (km / h) 7,5'},
    {Id: 4, Name: 'Mercedes-Benz E 300 d 4MATIC', Price: 52348, Description: 'Acceleration 0-100 (km / h) 6,3'},
    {Id: 5, Name: 'Mercedes-Benz E 400 d 4MATIC', Price: 57356, Description: 'Acceleration 0-100 (km / h) 5,1'},
    {Id: 6, Name: 'Mercedes-Benz E 200', Price: 43931, Description: 'Acceleration 0-100 (km / h) 7,5'},
    {Id: 7, Name: 'Mercedes-Benz E 200 4MATIC', Price: 46278, Description: 'Acceleration 0-100 (km / h) 7,6'},
    {Id: 8, Name: 'Mercedes-Benz E 300 ', Price: 49726, Description: 'Acceleration 0-100 (km / h) 6,2'},
    {Id: 9, Name: 'Mercedes-Benz E 300 de', Price: 50971, Description: 'Acceleration 0-100 (km / h) 5,9'},
    {Id: 10, Name: 'Mercedes-Benz E 300 de 4MATIC', Price: 53318, Description: 'Acceleration 0-100 (km / h) 5,9'},
    {Id: 11, Name: 'Mercedes-Benz E 300 e', Price: 49946, Description: 'Acceleration 0-100 (km / h) 5,7'},
    {Id: 12, Name: 'Mercedes-Benz E 300 e 4MATIC', Price: 52293, Description: 'Acceleration 0-100 (km / h) 5,8'},
    {Id: 13, Name: 'Mercedes-Benz E 450 4MATIC', Price: 57158, Description: 'Acceleration 0-100 (km / h) 5'},
    {Id: 14, Name: 'Mercedes-AMG E 53 4MATIC+', Price: 71859, Description: 'Acceleration 0-100 (km / h) 4,5'},
    {Id: 15, Name: 'Mercedes-AMG E 63 4MATIC', Price: 97768, Description: 'Acceleration 0-100 (km / h) 3,5'},
    {Id: 16, Name: 'Mercedes-AMG E 63 S 4MATIC ', Price: 107798, Description: 'Acceleration 0-100 (km / h) 3,4'}
]